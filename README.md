
# Flask Api Template

This repo provides the initial setup for development of a flask api with a PostgreSQL database. For development I provide a provisioning script for a vagrant machine with all the necessary dependencies.

---  

## Getting the source

*Flask Api Template* can be downloaded with the following command:

    $ git clone git://github.com/flask-api-template.git

For more information, please refer to the [git documentation](http://git-scm.com/documentation).


## Installation and Setup

*Flask Api Template* is based on Python and depends on the following major components:

* [PostgreSQL](http://www.postgresql.org/) database
* [Flask](http://flask.pocoo.org/) web application micro-framework for Python
* [SQLAlchemy](http://www.sqlalchemy.org/) ORM framework

To setup the development environment we use VirtualBox and vagrant:
* [Vagrant](https://www.vagrantup.com/) virtual environment manager
* [VirtualBox](https://www.virtualbox.org/)  virtualisation software

To start a project with flask-api-template:

1. If you don't have VirtualBox and Vagrant then install both
2. Navigate in the terminal to the directory you want to set up the project. For example:
  ```sh
    $ cd Desktop/
  ```
3. Chose a new name of your project and run
  ```sh
    $ git clone https://bitbucket.org/mauicv/flask-api-template.git new_name
  ```

First you'll want to rename some stuff:

- In the `Vagrantfile` change all mentions of `flask-api-template` to the new name of your project (same as the one chosen above). Lines 55, 103, 112 each contain mentions of `flask-api-template`:
  ```
  config.vm.synced_folder './', '/home/vagrant/flask-api-template'
  ```
  ```
  cd ~/flask-api-template
  ```
  ```
  echo "alias temp='cd ~/flask-api-template && source ./venv/bin/activate'" >> ~/.bashrc
  ```
  in each rename `flask-api-template` to the new name of the project.
- In `.env.example` file name the database something other than `db_url`. i.e.:
```
SQLALCHEMY_DATABASE_URI = postgresql:///db_url
SQLALCHEMY_DATABASE_URI_TEST = postgresql:///db_url
```
should become:
```
SQLALCHEMY_DATABASE_URI = postgresql:///new_name_url
SQLALCHEMY_DATABASE_URI_TEST = postgresql:///new_name_url_test
```
- Next in the `Vagrantfile` rename the database created in the following lines of the provisioning script to the same thing as in the `.env.example`:
  ```
  sudo -u vagrant createdb db_url
  sudo -u vagrant createdb db_url_test
  ```
should become:
  ```
  sudo -u vagrant createdb new_name_url
  sudo -u vagrant createdb new_name_url_test
  ```


Next to setup and start development:

- Make sure your in the folder containing the project:

```sh
$ cd new_name/
```

- create and provision the virtual machine:

```sh
$ vagrant up
```

- copy across environmental variables:

```sh
$ cp .env.example .env
```


## Run the server

1. In the directory you installed everything run: `vagrant ssh`. This will put you in the virtual machine.
2. In the virtual machine type: `temp` or whatever you renamed it. This will activate the python environment
3. To run the server type:

```sh
$ python run.py
```


## Database

To create the database with the relevant tables use:
```sh
$ python scripts.py mkdb
```


## Tests

for testing we use:
* [Pytest](https://docs.pytest.org/en/latest/) Testing framework
* [Factory-Boy](https://factoryboy.readthedocs.io/en/latest/)
fixture management tool

to run tests:

```sh
$ pytest -s
```


## Flake8

For linting we use:
* [Flake8](http://flake8.pycqa.org/en/latest/) Linting tool
