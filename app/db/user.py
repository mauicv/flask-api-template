"""Database models."""

from app.db import db
from app.db.common import CommonColumns

from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import INTEGER, TEXT


class ApiUser(db.Model, CommonColumns):
    """Api user table definition."""
    __tablename__ = 'api_user'
    id = Column(INTEGER, primary_key=True)
    name = Column(TEXT)


class Token(db.Model, CommonColumns):
    """Api user table definition."""
    __tablename__ = 'user_token'
    id = Column(INTEGER, primary_key=True)
    token = Column(TEXT)
