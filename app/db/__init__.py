from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

from app.db.user import ApiUser, Token # noqa
