from flask import Response, request
from functools import wraps
import json


def valid_credentials(token):
    """database token lookup and verify token is valid.

    WARNING: not implemented yet.
    """
    return True


def requires_auth(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if not request.data:
            return Response('incorrect token',
                            401,
                            {
                                'WWW-Authenticate': 'Basic realm="Login!"'
                            })
        data_form = json.loads(request.data)
        if not data_form.get('token', None) or not \
                valid_credentials(data_form['token']):
            return Response('incorrect token',
                            401,
                            {
                                'WWW-Authenticate': 'Basic realm="Login!"'
                            })
        return f(*args, **kwargs)
    return wrapper
