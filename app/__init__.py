from flask import Flask
from config import Config
from app.views.user_blueprint import user_blueprint
from app.db import db


def create_app(test_config=None):
    app = Flask(__name__)

    if test_config is None:
        app.config.from_object(Config)
    else:
        app.config.from_object(test_config)

    db.init_app(app)
    app.db = db

    app.register_blueprint(user_blueprint)

    return app
