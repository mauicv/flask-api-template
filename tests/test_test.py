
from tests.factories import UserFactory


def test_test(client):
    response = client.get("/")
    assert response.data.decode('ascii') == 'hello world'

    user = UserFactory()
    assert user.name == 'John'
