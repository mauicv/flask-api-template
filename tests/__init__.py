"""Unit and functional test suite for SkyLines."""

__all__ = ["setup_db", "setup_app", "teardown_db"]


def setup_db(app):
    """Method used to build a database"""
    app.db.create_all()


def teardown_db(app):
    """Method used to destroy a database"""
    app.db.session.remove()
    app.db.drop_all()
    app.db.session.bind.dispose()


def clean_db(app):
    """Clean all data, leaving schema as is
    Suitable to be run before each db-aware test. This is much faster than
    dropping whole schema an recreating from scratch.
    """
    for table in reversed(app.db.metadata.sorted_tables):
        app.db.session.execute(table.delete())
