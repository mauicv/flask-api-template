import pytest
from config import TestingConfig
from app import create_app
from tests import setup_db, teardown_db, clean_db


@pytest.fixture(scope="session")
def app():
    """Global application fixture
    Initialized with testing config file.
    """
    yield create_app(test_config=TestingConfig)


@pytest.fixture(scope="session")
def db(app):
    """Creates clean database schema and drops it on teardown
    Note, that this is a session scoped fixture, it will be executed only once
    and shared among all tests. Use `db_session` fixture to get clean database
    before each test.
    """

    setup_db(app)
    yield app.db
    teardown_db()


@pytest.fixture(scope="function")
def db_session(db, app):
    """Provides clean database before each test. After each test,
    session.rollback() is issued.
    Return sqlalchemy session.
    """

    with app.app_context():
        clean_db()
        yield db.session
        db.session.rollback()


@pytest.fixture
def client(app):
    with app.test_client(use_cookies=False) as client:
        yield client
