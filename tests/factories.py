import factory
from app.db import ApiUser


class UserFactory(factory.Factory):
    class Meta:
        model = ApiUser

    name = 'John'
