from os.path import join, dirname
from dotenv import load_dotenv
import sys

from app import create_app
app = create_app()
app.app_context().push()

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

if __name__ == '__main__':
    if len(sys.argv) >= 2:
        if sys.argv[1] == "help":
            print('\n')
            print('mkdb: drops and creates db')
            print('\n')

        if sys.argv[1] == "mkdb":
            app.db.drop_all()
            app.db.create_all()
