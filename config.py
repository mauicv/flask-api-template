"""Config."""
import os

from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)


class Config(object):
    """Config for flask."""
    if os.getenv('APP_SETTINGS') == 'production':
        SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")
        TESTING = False
        DEBUG = False
    elif os.getenv('APP_SETTINGS') == 'development':
        SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")
        TESTING = True
        DEBUG = True


class TestingConfig(object):
    """Configurations for Testing, with a separate test database."""

    SQLALCHEMY_DATABASE_URI = \
        os.environ.get("SQLALCHEMY_DATABASE_URI_TEST")
    TESTING = True
    DEBUG = True
